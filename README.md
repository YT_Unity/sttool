#STTool

##概述
Unity3D 脚本模板快速编辑，保存。脚本文件添加头注释，自动标注创建日期以及版权时间。

##问题
- 在使用Unity3D创建脚本文件时，希望Unity3D编辑器自动按照个人喜好预定义的模板来
   创建各类脚本文件，例如添加文件头注释等需求。
- 希望在脚本头注释中自动标注脚本创建日期，以及根据创建日期标注版权时间（例如：2017-2018）。

##条件
- 实际上，Unity3D将各类脚本文件模板放在了安装目录下（例如：Unity 5.4.1f1\Editor\Data\Resources\ScriptTemplates）。
- 在Unity3D提供的API中，可以通过AssetModificationProcessor类的OnWillCreateAsset
   方法捕获Unity3D编辑器创建Asset（包括脚本文件）事件。

##方案
###编辑
- 编写Unity3D扩展编辑器代码，通过继承EditorWindow绘制自定义编辑器窗口来编辑/保存Unity3D
   的各类脚本文件模板（文本文件）。
- 预定义标注字符串，“#CreateTime#”标注创建日期，“#CopyrightTime#”标注版权时间声明，
   在编辑模板时，在需要插入创建日期的地方使用“#CreateTime#”标注即可，插入版权时间同理。
- 编辑个人喜好风格的脚本文件模板，如下例C#脚本文件模板：

```
﻿/*************************************************************************
 *  Copyright (C), #CopyrightTime#, Mogoson tech. Co., Ltd.
 *  FileName: #SCRIPTNAME#.cs
 *  Author: Mogoson   Version: 1.0   Date: #CreateTime#
 *  Version Description:
 *    Internal develop version,mainly to achieve its function.
 *  File Description:
 *    Ignore.
 *  Class List:
 *    <ID>           <name>             <description>
 *     1.         #SCRIPTNAME#             Ignore.
 *  Function List:
 *    <class ID>     <name>             <description>
 *     1.
 *  History:
 *    <ID>    <author>      <time>      <version>      <description>
 *     1.     Mogoson     #CreateTime#       1.0        Build this file.
 *************************************************************************/

namespace Developer.Script
{
    using UnityEngine;
    using System.Collections;

    //[RequireComponent(typeof())]
    //[AddComponentMenu("")]
    public class #SCRIPTNAME# : MonoBehaviour
    {
        #region Property and Field
        //  [Tooltip("")]
        //  [SerializeField]
        #endregion

        #region Private Method
        //  void Start () 
        //  {
        //
        //  }

        //  void Update()
        //  {
        //
        //  }
        #endregion

        #region Public Method
        #endregion
    }//class_end
}//namespace_end
```

###创建
- 在Unity3D编辑器的Project面板通过鼠标右键菜单创建脚本文件（例如：C#）时，Unity3D引擎会将相应的
   模板文件里边的内容全部拷贝到新建的文件中，且自动将“#SCRIPTNAME#”替换为新建文件时输入的文件名。
   但是，“#CreateTime#”，“#CopyrightTime#”是我们自定义的标注字符串，Unity3D引擎不会识别，也不会
   将其替换成我们预期的内容。
- 编写扩展编辑器代码，继承AssetModificationProcessor类并实现其OnWillCreateAsset(string assetPath)
   方法以捕获Unity3D编辑器创建Asset（包括脚本文件）事件以及处理我们自定义的标注字符串。
- 捕获到创建Asset事件，检查文件扩展名，如果是.cs或.js或.shader或.compute则说明创建的是脚本文件，
  也就是说Unity3D引擎会按照相应的模板（例如C#，我们之前已经将其编辑成个人喜好风格的文本）来创建。
  （扩展编辑器代码）读取新建文件的内容，将“#CreateTime#”替换成当前日期，将#CopyrightTime#替换成
  当前年份-下个年份。

##实现
###扩展编辑器
- STEditor.cs 读取/编辑/保存 Unity3D各类脚本模板文件。
- STUpdater.cs 捕获创建事件，替换预定义标注字符串为其具体含义的内容。

###脚本模板示例
- Template 文件夹下存放各类脚本模板文件的示例，以供读者编写自己风格的模板时参考。